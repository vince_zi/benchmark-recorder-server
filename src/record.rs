use chrono::prelude::*;
use csv::Writer;
use rocket::response::status;
use std::error::Error;
use std::fs::OpenOptions;
use std::io::BufWriter;
use std::path::Path;

#[derive(Serialize, Deserialize)]
struct Record {
    id: i32,
    time: String,
}

fn write_record(id: i32) -> Result<(), Box<Error>> {
    let path = Path::new("resources/record.csv");

    let file = OpenOptions::new()
        .read(true)
        .append(true)
        .write(true)
        .create(true)
        .open(path)
        .unwrap();
    let mut wtr = Writer::from_writer(BufWriter::new(file));
    wtr.serialize((id, Local::now().format("%Y-%m-%d %H:%M:%S").to_string()))?;
    wtr.flush()?;
    Ok(())
}

#[put("/record/<id>")]
fn update_record(id: i32) -> status::NoContent {
    write_record(id).unwrap();
    status::NoContent
}

#![feature(plugin, decl_macro)]
#![plugin(rocket_codegen)]

#[macro_use]
extern crate serde_derive;

extern crate chrono;
extern crate csv;
extern crate rocket;
extern crate serde;
extern crate serde_json;

mod category;
mod record;
mod static_files;

fn main() {
    rocket::ignite()
        .mount("/", routes![static_files::files])
        .mount(
            "/api/v1",
            routes![category::get_category, record::update_record],
        )
        .launch();
}

use csv::Reader;
use rocket::response::content;
use serde_json::to_string;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;

#[derive(Serialize, Deserialize)]
struct Category {
    count: i32,
    name: String,
    id: i32,
}

fn read_category_file() -> Result<Vec<Category>, Box<Error>> {
    let path = Path::new("resources/category.csv");

    let file = File::open(&path).unwrap();
    let mut rdr = Reader::from_reader(BufReader::new(file));
    let mut categories = Vec::new();
    for result in rdr.deserialize() {
        let record: Category = result?;
        categories.push(record);
    }
    Ok(categories)
}

#[get("/category")]
fn get_category() -> content::Json<String> {
    let categories = read_category_file().unwrap();
    content::Json(to_string(&categories).unwrap())
}
